#include <stdio.h>
#include <math.h>

int main()
{

  double num;

  printf("Ingrese numero: ");
  scanf("%lf", &num);

  printf("Seno: %.3lf\n", sin(num));
  printf("Coseno: %.3lf\n", cos(num));
  printf("Tangente: %.3lf\n", tan(num));
  return 0;
}